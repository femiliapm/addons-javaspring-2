package shape;

import java.util.Scanner;

import shape.bangunDatar.Persegi;
import shape.bangunDatar.PersegiPanjang;
import shape.bangunRuang.Balok;
import shape.bangunRuang.Kubus;

public class Main {

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    try {
      System.out.println("===== MENU BANGUN =====");
      System.out.print("Input menu: ");
      String menu = input.nextLine();

      switch (menu) {
        case "1":
          // BangunDatar persegi = new Persegi();
          Persegi persegi = new Persegi();
          System.out.println(persegi.getShapeName());
          System.out.print("Input sisi: ");
          Double sisi = Double.valueOf(input.nextLine());
          persegi.setSisi(sisi);
          persegi.getInformation();
          break;

        case "2":
          PersegiPanjang persegiPanjang = new PersegiPanjang();
          System.out.println(persegiPanjang.getShapeName());
          System.out.print("Input panjang: ");
          Double panjang = Double.valueOf(input.nextLine());
          System.out.print("Input lebar: ");
          Double lebar = Double.valueOf(input.nextLine());
          persegiPanjang.setPanjang(panjang);
          persegiPanjang.setLebar(lebar);
          persegiPanjang.getInformation();
          break;

        case "3":
          Kubus kubus = new Kubus();
          System.out.println(kubus.getShapeName());
          System.out.print("Input sisi: ");
          sisi = Double.valueOf(input.nextLine());
          kubus.setSisi(sisi);
          kubus.getInformation();
          break;

        case "4":
          // instance object
          Balok balok = new Balok();
          // input atribut
          System.out.println(balok.getShapeName());
          System.out.print("Input panjang: ");
          panjang = Double.valueOf(input.nextLine());
          System.out.print("Input lebar: ");
          lebar = Double.valueOf(input.nextLine());
          System.out.print("Input tinggi: ");
          Double tinggi = Double.valueOf(input.nextLine());

          // set value
          balok.setPanjang(panjang);
          balok.setLebar(lebar);
          balok.setTinggi(tinggi);

          // print information
          balok.getInformation();
          break;

        default:
          System.out.println("Menu tidak tersedia!");
          break;
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      input.close();
    }
  }
}
